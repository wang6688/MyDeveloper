package com.wqg.mylop;

import cn.hutool.core.util.IdUtil;
import cn.hutool.db.ds.simple.SimpleDataSource;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.druid.DruidPlugin;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppendVideoData {

    @Test
    public void contextLoads() throws InterruptedException {

        for(int i  =0;i<3;i++){
            String coverPath = "/meta/cover/";
            String productId = "VC"+IdUtil.objectId().substring(0,15);
            coverPath+=productId.substring(productId.length()-4,productId.length());
            coverPath += "/"+productId+"/"+productId+".jpg";
            System.out.println(coverPath);
        }
    }
    @Test
    public  void  appendVideoData() {
        DataSource dataSource = initDBConnect("test");

        String videoTitle = "Engineering - Bridges By Design";
        String chineseTitle = "";
        String subTitle = "";
        String isrcode = "k4563";
        String author = "";
        String extent = "17";
        String introduce = "Engineers and architects are creating bridges that combine the best of both art and design. Bridges are no longer just a tool to get from one side of the river to the other. Some bridges have the goal of being aesthetically pleasing and efficient for pedestrians. Others rely on the cantilever design to span a distance equivalent to three and a half jumbo jets. Bridges are often classified by their structure and how the forces of tension, compression, bending, torsion and shear are distributed. In this program, students will learn how designs vary depending on the function of the bridge, the environmental factors, the materials, and technology used to construct them.";
        LocalDate pubDate = LocalDate.of(2011, 01, 01);
        int sonSum = 2;
        String keywords = "桥梁设计工程";
        String label = "课件、工程、桥梁设计";
        String currency ="USD";
        double price = 42.46;
        String videoAlbumContentId = IdUtil.simpleUUID();
        String productId = "VC"+IdUtil.objectId().substring(0,15);
        String coverPath = "/meta/cover/";
        coverPath+=productId.substring(productId.length()-4,productId.length());
        coverPath += "/"+productId+"/"+productId+".jpg";
        //-----------------------------------------
        Record videoRecord = new Record();
        videoRecord.set("CONTENT_ID",videoAlbumContentId);//'唯一标识'
        videoRecord.set("PRODUCT_ID",productId);//'资源唯一标识'
        videoRecord.set("ISBN",null);//'唯一标识'
        videoRecord.set("ISRC",isrcode);//'音像制品编码'
        videoRecord.set("VIDEO_TITLE",videoTitle);//'标题
        videoRecord.set("CONTENT_CHINESE_TITLE",chineseTitle);//'译名
        videoRecord.set("CONTENT_SUBTITLE",subTitle);//'副标题'
        videoRecord.set("AUTHOR",author);//作者、歌手、演讲者
        videoRecord.set("PUBLISHER_ID",null);//'出版商ID'
        videoRecord.set("PUB_LANG","English");//'出版语言
        videoRecord.set("CONTENT_PLACT","美国");//'出版地'
        videoRecord.set("SOURCE_ID",null);//'供应商'
        videoRecord.set("PUBDATE",pubDate);//'资源发布日期（出版日期）'
        videoRecord.set("AUDIO_ABSTRACT",introduce);//' 出版物摘要
        videoRecord.set("DURATION",extent);//'规模幅度(时长)'
        videoRecord.set("FORMAT","mp4");//'资源格式'
        videoRecord.set("SUBJECT_TYPE","VIDEO");//出版物分类，默认AUDIO'
        videoRecord.set("CATEGROY","Science");//'出版物原始分类subjectCode'
        videoRecord.set("SUBJECT_CLC_CN","V1 视频");//中图分类
        videoRecord.set("SUBJECT_CLC_EN","V1 Video");//中图分类-英文
        videoRecord.set("AUDIO_SON_NUM",sonSum);//'集数-集数为1 表示没有子集',
        videoRecord.set("COVER",coverPath);//'封面
        videoRecord.set("CONTENT_KEYWORDS",keywords);//'关键词
        videoRecord.set("CONTENT_LABEL",label);//'标签'
        videoRecord.set("PRICE",price);//码洋价
        videoRecord.set("CURRENCY",currency);//'码洋价币种'
        videoRecord.set("SALE_PRICE",null);//'销售价'
        videoRecord.set("SCURR",null);//'销售价币种'
        videoRecord.set("STATUS",0);//' '处理状态，0-未入库，1-已入库\r\n            原4期状态：1.已下架 2.已上架 10.下架中 20.上架中',
        videoRecord.set("CONTENT_WEB_URL",null);//'访问路径'
        videoRecord.set("CONTENT_EDITION",null);//'版本号'
        videoRecord.set("CONTENT_CHECKSTATUS",null);//'审读结果
        videoRecord.set("ISOA",1);//开源状态 0-否，1-是\r\n             原4期数据：1、不开源 2、开源',
        videoRecord.set("ISFREE",1);//免费状态 1、不免费 ；2、免费 改为 0-不免费，1-免费',
        videoRecord.set("ISLOCALE",0);//'是否本地 1-不是 2-是，改为0-否，1-是',
        videoRecord.set("CREATEDON",LocalDate.now());//'创建时间'
        videoRecord.set("CREATEDUSER","wwa");//'创建人
        videoRecord.set("UPDATEDON",null);//'修改人
        videoRecord.set("UPDATEDUSER",null);//'唯一标识'
        videoRecord.set("ISCHECK",0);//是否审读


        //-------------------------------------------------------------------------------
        Record videoOnSaleData = new Record();
        videoOnSaleData.setColumns(videoRecord);
        videoOnSaleData.remove("CONTENT_CHECKSTATUS");
        videoOnSaleData.remove("ISCHECK");
        videoOnSaleData.set("ISINSOLR","0");
//-------------------------------------------------------------------
        Record videoStreamStatus = new Record();
        videoStreamStatus.set("AUDIO_ID",videoAlbumContentId);  //主键
        videoStreamStatus.set("CONTENT_NET",1);  //所属站点类型
        videoStreamStatus.set("CONTENT_STATUS",2);  //1-未上架 2-已上架 3-已下架 4-上架中',
        videoStreamStatus.set("CONTENT_TYPE",14);  //资源类型',
        videoStreamStatus.set("CONTENT_AVAILABLE",null);  //下架原因分类\r\n            2-正常下架，3-政治原因 4-版权原因 5-主权声明',
//        `UNDER_NOTE` varchar(255) DEFAULT NULL COMMENT '下架原因',
//         `UPDATEDON` datetime DEFAULT NULL COMMENT '修改时间',
//         `UPDATEDUSER` varchar(50) DEFAULT NULL COMMENT '修改人',
//          `PRE_STATUS` int(11) DEFAULT NULL COMMENT '上一次操作状态',
//           `PRE_USER` varchar(32) DEFAULT NULL COMMENT '上一次操作人',
//           `PRE_TIME` datetime DEFAULT NULL COMMENT '上一次操作时间',
//           OPERATION_STATUS` int(11) DEFAULT NULL COMMENT '上下架操
        //---------------------------------------------------------------
        Db.tx(Connection.TRANSACTION_SERIALIZABLE, () -> {
            System.err.println("插入videoRecord:"+Db.save("pe_m_p_video", videoRecord));
            System.err.println("插入videoOnSaleData:"+Db.save("pe_m_p_on_sale_video", videoOnSaleData));
            System.err.println("插入videoOnSaleData:"+Db.save("pe_m_p_video_status", videoStreamStatus));

            appendVideoEPiData(videoOnSaleData);
            return true;
        });
    }

    public void appendVideoEPiData(Record onSaleVideoStream){


        String title = onSaleVideoStream.getStr("CONTENT_TITLE") ;
        String parentContentId = onSaleVideoStream.getStr("CONTENT_ID");
        String format = onSaleVideoStream.getStr("FORMAT") ;
        String introduce = onSaleVideoStream.getStr("AUDIO_ABSTRACT") ;
        String keywords = onSaleVideoStream.getStr("CONTENT_KEYWORDS") ;
        String label = onSaleVideoStream.getStr("CONTENT_LABEL") ;
        int sonNum = onSaleVideoStream.getInt("AUDIO_SON_NUM") ;
        String publishDate = onSaleVideoStream.getStr("PUBDATE") ;
        String pubLang = onSaleVideoStream.getStr("PUB_LANG") ;
        String plact = onSaleVideoStream.getStr("CONTENT_PLACT") ;
    for(int i = 0;i<sonNum;i++){
        String contentId = IdUtil.simpleUUID();
        String productId = "VE"+IdUtil.simpleUUID().substring(0,15);
       // String isrcode = "ISRC"+IdUtil.objectId();
        String coverPath = "/meta/cover/";
        coverPath+=productId.substring(productId.length()-4,productId.length());
        coverPath += "/"+productId+"/"+productId+".jpg";
        Record epi4VideoRecord = new Record();
        epi4VideoRecord.set("EPISODE_ID",contentId);   //主键=七牛ID
        epi4VideoRecord.set("PRODUCT_ID",productId);   //资源唯一标识
        epi4VideoRecord.set("CONTENT_ID",parentContentId);   //父出版物
        epi4VideoRecord.set("TITLE",title);   //标题
        epi4VideoRecord.set("CHINESE_TITLE",null);   //译名
        epi4VideoRecord.set("SUBTITLE",null);   //副标题
        epi4VideoRecord.set("AUTHOR",null);   //作者、歌手、演讲者',
        epi4VideoRecord.set("PUBDATE",publishDate);   //资源发布日期（出版日期）
        epi4VideoRecord.set("PUB_LANG",pubLang);   //语种',
        epi4VideoRecord.set("CONTENT_PLACT",plact);   //出版地
        epi4VideoRecord.set("FORMAT",format);   //资源格式
        epi4VideoRecord.set("EXTENT",null);   //规模幅度
        epi4VideoRecord.set("SORT_CODE",i+1);   //节序点
        epi4VideoRecord.set("EPISODE_ABSTRACT",introduce);   //分集简介
        epi4VideoRecord.set("CONTENT_KEYWORDS",keywords);   //关键词
        epi4VideoRecord.set("CONTENT_LABEL",label);   //标签
        epi4VideoRecord.set("CONTENT_CHECKSTATUS","0");   //审读结果
        epi4VideoRecord.set("COVER_PATH",coverPath);   //封面路径
        epi4VideoRecord.set("CREATEDON",LocalDate.now());   //创建时间
        epi4VideoRecord.set("CREATEDUSER","wwa");   //资源格式
        epi4VideoRecord.set("UPDATEDON",null);   //资源格式
        epi4VideoRecord.set("UPDATEDUSER",null);   //资源格式
        epi4VideoRecord.set("COVER",coverPath);   //封面截屏
        epi4VideoRecord.set("ISOA","1");   //
        epi4VideoRecord.set("ISFREE","1");   //免费状态 0-不免费,1-免费'


        Record onSaleVideoEpiRecord = new Record();
        onSaleVideoEpiRecord.setColumns(epi4VideoRecord);
        onSaleVideoEpiRecord.remove("CONTENT_CHECKSTATUS");
        onSaleVideoEpiRecord.set("ISINSOLR",0);


        //--------------------------------------------------------------
        Record voiceEpi4StatusRecord = new Record();
        voiceEpi4StatusRecord.set("CONTENT_ID",contentId);//主键
        voiceEpi4StatusRecord.set("CONTENT_NET","1");       //所属站点类型',
        voiceEpi4StatusRecord.set("CONTENT_STATUS","2");  //'1-未上架 2-已上架 3-已下架 4-上架中',
        voiceEpi4StatusRecord.set("CONTENT_TYPE","16");           //资源类型

//                `CONTENT_AVAILABLE` int(11) DEFAULT NULL COMMENT '下架原因分类：2-正常下架，3-政治原因 4-版权原因 5-主权声明',
//                `UNDER_NOTE` varchar(255) DEFAULT NULL COMMENT '下架原因',
//                `UPDATEDON` datetime DEFAULT NULL COMMENT '修改时间',
//                `UPDATEDUSER` varchar(50) DEFAULT NULL COMMENT '修改人',
//                `PRE_STATUS` int(11) DEFAULT NULL COMMENT '上一次操作状态',
//                `PRE_USER` varchar(32) DEFAULT NULL COMMENT '上一次操作人',
//                `PRE_TIME` datetime DEFAULT NULL COMMENT '上一次操作时间',
//                `OPERATION_STATUS` int(11) DEFAULT NULL COMMENT '上下架操作

        Db.tx(Connection.TRANSACTION_SERIALIZABLE,()->{
            System.out.println("epi4VideoRecord:"+Db.save("pe_m_p_video_episode",epi4VideoRecord));
            System.out.println("onSaleVideoEpiRecord:"+Db.save("pe_m_p_on_sale_video_episode",onSaleVideoEpiRecord));
            System.out.println("voiceEpi4StatusRecord:"+Db.save("pe_m_p_video_episode_status", voiceEpi4StatusRecord));
            return true;
        });
    }


    }



    public static DataSource initDBConnect(String dbSource){
        String jdbcUrl ="jdbc:mysql://127.0.0.1:3306/cnpedb?verifyServerCertificate=false&useSSL=false&requireSSL=false&useUnicode=true&characterEncoding=UTF-8&rewriteBatchedStatements=true&useAffectedRows=true\n";
        if("local".equals(dbSource)){
        }
        if("test".equals(dbSource)){
            jdbcUrl="jdbc:mysql://192.168.10.111:3306/cnpedb?verifyServerCertificate=false&useSSL=false&requireSSL=false&useUnicode=true&characterEncoding=UTF-8&rewriteBatchedStatements=true&useAffectedRows=true\n";
        }
        DruidPlugin dp = new DruidPlugin(jdbcUrl, "root", "123456");
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        // arp.addMapping("blog", Blog.class);

        // 与 jfinal web 环境唯一的不同是要手动调用一次相关插件的start()方法
        dp.start();
        arp.start();
        return  new SimpleDataSource(jdbcUrl,"root","123456");
    }

}
