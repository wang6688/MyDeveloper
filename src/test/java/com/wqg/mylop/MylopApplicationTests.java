package com.wqg.mylop;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.db.ds.simple.SimpleDataSource;
import cn.hutool.db.meta.MetaUtil;
import cn.hutool.db.meta.Table;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.druid.DruidPlugin;
import com.sun.corba.se.impl.presentation.rmi.IDLTypesUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.time.LocalDate;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MylopApplicationTests {

    @Test
    public void contextLoads() {
    }
    @Test
    public  void  appendVoiceData() {
        DataSource dataSource = initDBConnect("test");

        // 通过上面简单的几行代码，即可立即开始使用
        //   new Blog().set("title", "title").set("content", "cxt text").save();
        //Blog.dao.findById(123);
        Record voiceMasterRecord = new Record();
        String audioTitle = "Closer to God";
        String contentId = IdUtil.simpleUUID();
        String productId = "AS"+IdUtil.objectId().substring(0,15);
        String isrcode = "ISRC"+ IdUtil.objectId();
        System.out.println("'资源记录主键':"+voiceMasterRecord.set("CONTENT_ID", contentId));
        System.out.println("''中图唯一ID 前缀AS'':"+voiceMasterRecord.set("PRODUCT_ID", productId));
        System.out.println("''出版社ID'':"+voiceMasterRecord.set("PUBLISHER_ID", IdUtil.simpleUUID()));
        System.out.println("''供应商ID'':"+voiceMasterRecord.set("SOURCE_ID", IdUtil.simpleUUID()));
        System.out.println("音频标准编码"+voiceMasterRecord.set("CONTENT_ISRC",isrcode));
        System.out.println("'标题 ':"+voiceMasterRecord.set("CONTENT_TITLE", audioTitle));
        System.out.println("幅标标题 :"+voiceMasterRecord.set("CONTENT_SUBTITLE", audioTitle));
        System.out.println("'中文译标题':"+voiceMasterRecord.set("CONTENT_CHINESE_TITLE",audioTitle));
        System.out.println("'简介':"+voiceMasterRecord.set("CONTENT_ABSTRACT", audioTitle+"的歌曲简介"));
        System.out.println("'语言':"+voiceMasterRecord.set("CONTENT_LANGUAGE", "chs"));
        System.out.println("'唱片公司':"+voiceMasterRecord.set("CONTENT_COMPANY", "北京可以这样有限公司"));
        System.out.println("品牌:"+voiceMasterRecord.set("CONTENT_BRAND", "未知"));
        System.out.println("作曲家:"+voiceMasterRecord.set("CONTENT_COMPOSER", "张三"));
        System.out.println("'指挥家':"+voiceMasterRecord.set("CONTENT_CONDUCTOR", "李四"));
        System.out.println("'演奏家':"+voiceMasterRecord.set("CONTENT_PERFORMER", "王五"));
        System.out.println("'歌唱家':"+voiceMasterRecord.set("CONTENT_SINGER", "赵六"));
        System.out.println("'乐队':"+voiceMasterRecord.set("CONTENT_BAND", "小鸟乐队"));
        System.out.println("'表演者':"+voiceMasterRecord.set("CONTENT_ENTERTAINER", "小七"));
        System.out.println("'审读结果':"+voiceMasterRecord.set("CONTENT_CHECKSTATUS", "1"));
        System.out.println("入库状态:"+voiceMasterRecord.set("CONTENT_STATUS", "1"));
        System.out.println("'出版日期':"+voiceMasterRecord.set("CONTENT_PUB_DATE", LocalDate.now()));
        System.out.println("'中图分类-中文':"+voiceMasterRecord.set("SUBJECT_CLC_CN", "J6 音乐"));
        System.out.println("'中图分类-英文':"+voiceMasterRecord.set("SUBJECT_CLC_EN", "J6 Music"));
        System.out.println("免费状态 0-不免费,1-免费:"+voiceMasterRecord.set("IS_FREE", "0"));
        System.out.println("是否本地:"+voiceMasterRecord.set("IS_LOCAL", "0"));
        System.out.println("'创建时间':"+voiceMasterRecord.set("CREATEDON", LocalDate.now()));
        System.out.println("'创建人':"+voiceMasterRecord.set("CREATEDUSER", "wwa"));
        System.out.println("'修改时间':"+voiceMasterRecord.set("UPDATEDON", LocalDate.now()));
        System.out.println("'修改人':"+voiceMasterRecord.set("UPDATEDUSER", "wwa"));
        //System.err.println(Db.save("PE_M_P_AUDIOSTREAM", voiceMasterRecord));
        //-----------------------------------------

        Record voiceAtt1Data = new Record();
        System.out.println("'唯一标识'"+voiceAtt1Data.set("CONTENT_ID", contentId));
        System.out.println("'出版物原始分类subjectCode'"+voiceAtt1Data.set("CONTENT_CATEGROY", "AUDIO"));
        System.out.println("'规模幅度(时长)' "+voiceAtt1Data.set("CONTENT_DURATION", "20:35:33"));
        System.out.println("作品时代"+voiceAtt1Data.set("CONTENT_AGE", "古典主义"));
        System.out.println("'出版地'"+voiceAtt1Data.set("CONTENT_PLACT", "中国"));
        System.out.println("'封面'"+voiceAtt1Data.set("CONTENT_COVER", "暂无"));
        System.out.println("'封底'"+voiceAtt1Data.set("CONTENT_BACK_COVER", "暂无"));
        System.out.println("资源格式"+voiceAtt1Data.set("CONTENT_FORMAT", "CD"));
        System.out.println("'集数'"+voiceAtt1Data.set("CONTENT_SON_NUM", "20"));
        System.out.println("'自定义关键词'"+voiceAtt1Data.set("CONTENT_KEYWORDS", "unkonw"));
        System.out.println("'自定义标签'"+voiceAtt1Data.set("CONTENT_LABEL", "unknow"));
        System.out.println("'外部WEB访问路径'"+voiceAtt1Data.set("CONTENT_WEB_URL", "http://www.baidu.com"));
        System.out.println("电子价"+voiceAtt1Data.set("CONTENT_EPRICE", "100"));
        System.out.println("零售价"+voiceAtt1Data.set("CONTENT_LPRICE", "120"));
        //  System.out.println("'海外售价'"+voiceAtt1Data.put("'CONTENT_FPRICE'", "200"));
        System.out.println("码洋价币种"+voiceAtt1Data.set("CONTENT_CURRENCY", "rmb"));
        System.out.println("'更新时间'"+voiceAtt1Data.set("UPDATEDON", LocalDate.now()));
       // System.err.println("插入audioAttach2"+Db.save("PE_M_P_AUDIOSTREAM2", voiceAtt1Data));
//-----------------------------------------------------------------
        Record voiceAtt2Data = new Record();
        System.out.println("'唯一标识'"+voiceAtt2Data.set("CONTENT_ID", contentId));
        System.out.println("可采购"+voiceAtt2Data.set("IS_BUYABLE", 0));
        System.out.println("可销售 "+voiceAtt2Data.set("IS_SALEABLE", 1));
        System.out.println("name"+voiceAtt2Data.set("CONTENT_BTRADE_CLASSIFY", "unknow"));
        System.out.println("name"+voiceAtt2Data.set("CONTENT_BTRADE_CLASSIFY_NO", "unknow"));
        System.out.println("name"+voiceAtt2Data.set("CONTENT_MUSIC_CLASSIFY", "unknow"));
        System.out.println("name"+voiceAtt2Data.set("UPDATEDON",LocalDate.now()));
        //System.err.println(Db.save("PE_M_P_AUDIOSTREAM3", voiceAtt2Data));
        //-------------------------------------------------------------------------------
        Record voiceOnSaleData = new Record();
        System.out.println("'资源记录主键':"+voiceOnSaleData.set("CONTENT_ID", contentId));
        System.out.println("''中图唯一ID 前缀AS'':"+voiceOnSaleData.set("PRODUCT_ID", productId));
        System.out.println("''出版社ID'':"+voiceOnSaleData.set("PUBLISHER_ID", IdUtil.simpleUUID()));
        System.out.println("''供应商ID'':"+voiceOnSaleData.set("SOURCE_ID", IdUtil.simpleUUID()));
        System.out.println("音频标准编码"+voiceOnSaleData.set("CONTENT_ISRC",isrcode));
        System.out.println("'标题 ':"+voiceOnSaleData.set("CONTENT_TITLE", audioTitle));
        System.out.println("幅标标题 :"+voiceOnSaleData.set("CONTENT_SUBTITLE", audioTitle));
        System.out.println("'中文译标题':"+voiceOnSaleData.set("CONTENT_CHINESE_TITLE",audioTitle));
        System.out.println("'简介':"+voiceOnSaleData.set("CONTENT_ABSTRACT", audioTitle+"的歌曲简介"));
        System.out.println("'语言':"+voiceOnSaleData.set("CONTENT_LANGUAGE", "chs"));
        System.out.println("'唱片公司':"+voiceOnSaleData.set("CONTENT_COMPANY", "北京可以这样有限公司"));
        System.out.println("品牌:"+voiceOnSaleData.set("CONTENT_BRAND", "未知"));
        System.out.println("作曲家:"+voiceOnSaleData.set("CONTENT_COMPOSER", "张三"));
        System.out.println("'指挥家':"+voiceOnSaleData.set("CONTENT_CONDUCTOR", "李四"));
        System.out.println("'演奏家':"+voiceOnSaleData.set("CONTENT_PERFORMER", "王五"));
        System.out.println("'歌唱家':"+voiceOnSaleData.set("CONTENT_SINGER", "赵六"));
        System.out.println("'乐队':"+voiceOnSaleData.set("CONTENT_BAND", "花儿乐队"));
        System.out.println("'表演者':"+voiceOnSaleData.set("CONTENT_ENTERTAINER", "小七"));
       // System.out.println("入库状态:"+voiceOnSaleData.set("CONTENT_STATUS", "1"));
        System.out.println("'出版日期':"+voiceOnSaleData.set("CONTENT_PUB_DATE", LocalDate.now()));
        System.out.println("'中图分类-中文':"+voiceOnSaleData.set("SUBJECT_CLC_CN", "J6 音乐"));
        System.out.println("'中图分类-英文':"+voiceOnSaleData.set("SUBJECT_CLC_EN", "J6 Music"));
        System.out.println("免费状态 0-不免费,1-免费:"+voiceOnSaleData.set("IS_FREE", "0"));
        System.out.println("是否本地:"+voiceOnSaleData.set("IS_LOCAL", "0"));
        System.out.println("'出版物原始分类subjectCode'"+voiceOnSaleData.set("CONTENT_CATEGROY", "AUDIO"));
        System.out.println("'规模幅度(时长)'"+voiceOnSaleData.set("CONTENT_DURATION", "20:35:33"));
        System.out.println("作品时代"+voiceOnSaleData.set("CONTENT_AGE", "古典主义"));
        System.out.println("'出版地'"+voiceOnSaleData.set("CONTENT_PLACT", "中国"));
        System.out.println("'封面'"+voiceOnSaleData.set("CONTENT_COVER", "暂无"));
        System.out.println("'封底'"+voiceOnSaleData.set("CONTENT_BACK_COVER", "暂无"));
        System.out.println("资源格式"+voiceOnSaleData.set("CONTENT_FORMAT", "CD"));
        System.out.println("'集数'"+voiceOnSaleData.set("CONTENT_SON_NUM", "20"));
        System.out.println("'自定义关键词'"+voiceOnSaleData.set("CONTENT_KEYWORDS", "unkonw"));
        System.out.println("'自定义标签'"+voiceOnSaleData.set("CONTENT_LABEL", "unknow"));
        System.out.println("'外部WEB访问路径'"+voiceOnSaleData.set("CONTENT_WEB_URL", ""));
        System.out.println("电子价"+voiceOnSaleData.set("CONTENT_EPRICE", "100"));
        System.out.println("零售价"+voiceOnSaleData.set("CONTENT_LPRICE", "120"));
        //  System.out.println("'海外售价'"+voiceOnSaleData.set("'CONTENT_FPRICE'", "200"));
        System.out.println("码洋价币种"+voiceOnSaleData.set("CONTENT_CURRENCY", "rmb"));
        System.out.println("'创建时间':"+voiceOnSaleData.set("CREATEDON", LocalDate.now()));
        System.out.println("'创建人':"+voiceOnSaleData.set("CREATEDUSER", "wwa"));
        System.out.println("'修改时间':"+voiceOnSaleData.set("UPDATEDON", LocalDate.now()));
        System.out.println("'修改人':"+voiceOnSaleData.set("UPDATEDUSER", "wwa"));
        System.out.println("zai solr "+voiceOnSaleData.set("ISINSOLR", "0"));
       // Db.save("PE_M_P_ON_SALE_AUDIOSTREAM",voiceOnSaleData);
//-------------------------------------------------------------------
        Record audioStreamStatus = new Record();
        audioStreamStatus.set("AUDIO_ID",contentId);
        System.out.println("'所属站点类型'"+audioStreamStatus.set("CONTENT_NET", 1));
        System.out.println("1-未上架 2-已上架 3-已下架 4-上架中"+audioStreamStatus.set("CONTENT_STATUS", 1));
        System.out.println("'资源类型'"+audioStreamStatus.set("CONTENT_TYPE", 13));
        System.out.println("'下架原因分类 2-正常下架，3-政治原因 4-版权原因 5-主权声明'"+audioStreamStatus.set("CONTENT_AVAILABLE", 1));
        System.out.println("'下架原因"+audioStreamStatus.set("UNDER_NOTE",null));
        System.out.println("修改时间"+audioStreamStatus.set("UPDATEDON",null));
                //`UPDATEDUSER` varchar(50) DEFAULT NULL COMMENT '修改人',
              // `PRE_STATUS` int(11) DEFAULT NULL COMMENT '上一次操作状态',
             //   `PRE_USER` varchar(32) DEFAULT NULL COMMENT '上一次操作人',
             //   `PRE_TIME` datetime DEFAULT NULL COMMENT '上一次操作时间',
            //    `OPERATION_STATUS` int(11) DEFAULT NULL COMMENT '上下架操作状态 1-成功，2-失败',
        //---------------------------------------------------------------
        Db.tx(Connection.TRANSACTION_SERIALIZABLE, () -> {
            System.err.println(Db.save("PE_M_P_AUDIOSTREAM", voiceMasterRecord));
            System.err.println("插入audioAttach2"+Db.save("PE_M_P_AUDIOSTREAM2", voiceAtt1Data));
            System.err.println(Db.save("PE_M_P_AUDIOSTREAM3", voiceAtt2Data));
            Db.save("PE_M_P_ON_SALE_AUDIOSTREAM",voiceOnSaleData);
            Db.save("pe_m_p_audiostream_status",audioStreamStatus);
            appendVoiceEPiData(voiceOnSaleData);
            return true;
        });
    }

    public void appendVoiceEPiData(Record onSaleAudioStream){

        String contentId = IdUtil.simpleUUID();
        String productId = "SE"+IdUtil.objectId().substring(0,15);
        String isrcode = "ISRC"+IdUtil.objectId();
        String title = onSaleAudioStream.getStr("CONTENT_TITLE") ;
        String parentContentId = onSaleAudioStream.getStr("CONTENT_ID");

        Record epi4VoiceRecord = new Record();
        System.out.println("资源记录主键"+epi4VoiceRecord.set("CONTENT_ID", contentId));
        System.out.println("chanpindid"+epi4VoiceRecord.set("PRODUCT_ID", productId));
        System.out.println("专辑ID"+epi4VoiceRecord.set("CONTENT_PARENT_ID", parentContentId));
        System.out.println("音频标准编码"+epi4VoiceRecord.set("CONTENT_ISRC", isrcode));
        System.out.println("标题"+epi4VoiceRecord.set("CONTENT_TITLE", onSaleAudioStream.getStr("CONTENT_TITLE")));
        System.out.println("幅标标题"+epi4VoiceRecord.set("CONTENT_SUBTITLE",onSaleAudioStream.getStr("CONTENT_SUBTITLE")));
        System.out.println("中文译标题"+epi4VoiceRecord.set("CONTENT_CHINESE_TITLE",onSaleAudioStream.getStr("CONTENT_CHINESE_TITLE")));
        System.out.println("简介"+epi4VoiceRecord.set("CONTENT_ABSTRACT", onSaleAudioStream.getStr("CONTENT_ABSTRACT")));
        System.out.println("语言"+epi4VoiceRecord.set("CONTENT_LANGUAGE", onSaleAudioStream.getStr("CONTENT_LANGUAGE")));
        System.out.println("作曲家"+epi4VoiceRecord.set("CONTENT_COMPOSER",onSaleAudioStream.getStr("CONTENT_COMPOSER")));
        System.out.println("指挥家"+epi4VoiceRecord.set("CONTENT_CONDUCTOR", onSaleAudioStream.getStr("CONTENT_CONDUCTOR")));
        System.out.println("演奏家"+epi4VoiceRecord.set("CONTENT_PERFORMER", onSaleAudioStream.getStr("CONTENT_PERFORMER")));
        System.out.println("歌唱家"+epi4VoiceRecord.set("CONTENT_SINGER", onSaleAudioStream.getStr("CONTENT_SINGER")));
        System.out.println("乐队"+epi4VoiceRecord.set("CONTENT_BAND", onSaleAudioStream.getStr("CONTENT_BAND")));
        System.out.println("表演者"+epi4VoiceRecord.set("CONTENT_ENTERTAINER", onSaleAudioStream.getStr("CONTENT_ENTERTAINER")));
        System.out.println("审读结果"+epi4VoiceRecord.set("CONTENT_CHECKSTATUS", "0"));
        System.out.println("中图分类-中文"+epi4VoiceRecord.set("SUBJECT_CLC_CN", onSaleAudioStream.getStr("SUBJECT_CLC_CN")));
        System.out.println("中图分类-英文"+epi4VoiceRecord.set("SUBJECT_CLC_EN",onSaleAudioStream.getStr("SUBJECT_CLC_EN")));
       // System.out.println("tempadd"+epi4VoiceRecord.set("testadd", "0"));
        System.out.println("免费状态"+epi4VoiceRecord.set("IS_FREE", onSaleAudioStream.getStr("IS_FREE")));
        System.out.println("原始分类"+epi4VoiceRecord.set("CONTENT_CATEGROY", onSaleAudioStream.getStr("CONTENT_CATEGROY")));
        System.out.println("规模幅度"+epi4VoiceRecord.set("CONTENT_DURATION", "2:35"));
        System.out.println("作品时代"+epi4VoiceRecord.set("CONTENT_AGE", onSaleAudioStream.getStr("CONTENT_AGE")));
        System.out.println("出版地"+epi4VoiceRecord.set("CONTENT_PLACT",onSaleAudioStream.getStr("CONTENT_PLACT")));
        System.out.println("资源格式"+epi4VoiceRecord.set("CONTENT_FORMAT", onSaleAudioStream.getStr("CONTENT_FORMAT")));
        //  System.out.println("关键词"+epi4VoiceRecord.set("CONTENT_KEYWORDS", "unknow"));
        System.out.println("标签"+epi4VoiceRecord.set("CONTENT_LABEL",onSaleAudioStream.getStr("CONTENT_LABEL")));
        System.out.println("电子价"+epi4VoiceRecord.set("CONTENT_EPRICE", onSaleAudioStream.getStr("CONTENT_EPRICE")));
        System.out.println("海外售价"+epi4VoiceRecord.set("CONTENT_FPRICE",onSaleAudioStream.getStr("CONTENT_FPRICE")));
        System.out.println("海外价币种"+epi4VoiceRecord.set("CONTENT_FCURRENCY",onSaleAudioStream.getStr("CONTENT_FCURRENCY")));
        System.out.println("零售价"+epi4VoiceRecord.set("CONTENT_LPRICE", onSaleAudioStream.getStr("CONTENT_LPRICE")));
        System.out.println("币种"+epi4VoiceRecord.set("CONTENT_CURRENCY", onSaleAudioStream.getStr("CONTENT_CURRENCY")));
        System.out.println("外部访问路径"+epi4VoiceRecord.set("CONTENT_WEB_URL", onSaleAudioStream.getStr("CONTENT_WEB_URL")));
        System.out.println("物理文件路径"+epi4VoiceRecord.set("CONTENT_PATH",onSaleAudioStream.getStr("CONTENT_PATH")));
       // System.out.println("addtest"+epi4VoiceRecord.set("add", ""));
        System.out.println("排序"+epi4VoiceRecord.set("CONTENT_SORT", 1));
        epi4VoiceRecord.set("CREATEDON",LocalDate.now());
        epi4VoiceRecord.set("CREATEDUSER","wwa");
        epi4VoiceRecord.set("UPDATEDON",LocalDate.now());
        epi4VoiceRecord.set("UPDATEDUSER","wwa");

      
        Record onSaleVoiceEpiRecord = new Record();
        onSaleVoiceEpiRecord.set("CONTENT_ID",epi4VoiceRecord.get("CONTENT_ID"));
        onSaleVoiceEpiRecord.set("PRODUCT_ID",epi4VoiceRecord.get("PRODUCT_ID"));
        onSaleVoiceEpiRecord.set("CONTENT_PARENT_ID",epi4VoiceRecord.get("CONTENT_PARENT_ID"));
        onSaleVoiceEpiRecord.set("CONTENT_ISRC",epi4VoiceRecord.get("CONTENT_ISRC"));
        onSaleVoiceEpiRecord.set("CONTENT_TITLE",epi4VoiceRecord.get("CONTENT_TITLE"));
        onSaleVoiceEpiRecord.set("CONTENT_SUBTITLE",epi4VoiceRecord.get("CONTENT_SUBTITLE"));
        onSaleVoiceEpiRecord.set("CONTENT_CHINESE_TITLE",epi4VoiceRecord.get("CONTENT_CHINESE_TITLE",""));
        onSaleVoiceEpiRecord.set("CONTENT_ABSTRACT",epi4VoiceRecord.get("CONTENT_ABSTRACT",""));
        onSaleVoiceEpiRecord.set("CONTENT_LANGUAGE",epi4VoiceRecord.get("CONTENT_LANGUAGE",""));
        onSaleVoiceEpiRecord.set("CONTENT_COMPOSER",epi4VoiceRecord.get("CONTENT_COMPOSER",""));
        onSaleVoiceEpiRecord.set("CONTENT_CONDUCTOR",epi4VoiceRecord.get("CONTENT_CONDUCTOR",""));
        onSaleVoiceEpiRecord.set("CONTENT_PERFORMER",epi4VoiceRecord.get("CONTENT_PERFORMER",""));
        onSaleVoiceEpiRecord.set("CONTENT_SINGER",epi4VoiceRecord.get("CONTENT_SINGER",""));
        onSaleVoiceEpiRecord.set("CONTENT_BAND",epi4VoiceRecord.get("CONTENT_BAND",""));
        onSaleVoiceEpiRecord.set("CONTENT_ENTERTAINER",epi4VoiceRecord.get("CONTENT_ENTERTAINER",""));
        // onSaleVoiceEpiRecord.set("CONTENT_CHECKSTATUS",epi4VoiceRecord.get("CONTENT_CHECKSTATUS",""));
        //   onSaleVoiceEpiRecord.set("CONTENT_STATUS",epi4VoiceRecord.get("CONTENT_STATUS","1"));
        onSaleVoiceEpiRecord.set("SUBJECT_CLC_CN",epi4VoiceRecord.get("SUBJECT_CLC_CN",""));
        onSaleVoiceEpiRecord.set("SUBJECT_CLC_EN",epi4VoiceRecord.get("SUBJECT_CLC_EN",""));
        onSaleVoiceEpiRecord.set("IS_FREE",epi4VoiceRecord.get("IS_FREE",""));
        onSaleVoiceEpiRecord.set("CONTENT_DURATION",epi4VoiceRecord.get("CONTENT_DURATION",""));
        onSaleVoiceEpiRecord.set("CONTENT_AGE",epi4VoiceRecord.get("CONTENT_AGE",""));
        onSaleVoiceEpiRecord.set("CONTENT_PLACT",epi4VoiceRecord.get("CONTENT_PLACT",""));
        onSaleVoiceEpiRecord.set("CONTENT_FORMAT",epi4VoiceRecord.get("CONTENT_FORMAT",""));
        onSaleVoiceEpiRecord.set("CONTENT_KEYWORDS",epi4VoiceRecord.get("CONTENT_KEYWORDS",""));
        onSaleVoiceEpiRecord.set("CONTENT_LABEL",epi4VoiceRecord.get("CONTENT_LABEL",""));
        onSaleVoiceEpiRecord.set("CONTENT_EPRICE",epi4VoiceRecord.get("CONTENT_EPRICE",""));
        onSaleVoiceEpiRecord.set("CONTENT_LPRICE",epi4VoiceRecord.get("CONTENT_LPRICE",""));
        onSaleVoiceEpiRecord.set("CONTENT_CURRENCY",epi4VoiceRecord.get("CONTENT_CURRENCY",""));
        onSaleVoiceEpiRecord.set("CONTENT_SORT",epi4VoiceRecord.get("CONTENT_SORT",""));
        onSaleVoiceEpiRecord.set("CREATEDON",epi4VoiceRecord.get("CREATEDON",""));
        onSaleVoiceEpiRecord.set("CREATEDUSER",epi4VoiceRecord.get("CREATEDUSER",""));
        onSaleVoiceEpiRecord.set("UPDATEDON",epi4VoiceRecord.get("UPDATEDON",""));
        onSaleVoiceEpiRecord.set("UPDATEDUSER",epi4VoiceRecord.get("UPDATEDUSER",""));
        onSaleVoiceEpiRecord.set("ISINSOLR",epi4VoiceRecord.get("ISINSOLR","0"));


        //--------------------------------------------------------------
        Record voiceEpi4StatusRecord = new Record();
        voiceEpi4StatusRecord.set("CONTENT_ID",epi4VoiceRecord.get("CONTENT_ID"));
        System.out.println("'所属站点类型'"+voiceEpi4StatusRecord.set("CONTENT_NET", 1));
        System.out.println("'所属站点类型'1-未上架 2-已上架 3-已下架 4-上架中"+voiceEpi4StatusRecord.set("CONTENT_STATUS", 1));
        System.out.println("'资源类型'"+voiceEpi4StatusRecord.set("CONTENT_TYPE", 15));
//        `CONTENT_AVAILABLE` int(11) DEFAULT NULL COMMENT '下架原因分类：2-正常下架，3-政治原因 4-版权原因 5-主权声明',
//        `UNDER_NOTE` varchar(255) DEFAULT NULL COMMENT '下架原因',
//        `UPDATEDON` datetime DEFAULT NULL COMMENT '修改时间',
//        `UPDATEDUSER` varchar(50) DEFAULT NULL COMMENT '修改人',
//        `PRE_STATUS` int(11) DEFAULT NULL COMMENT '上一次操作状态',
//        `PRE_USER` varchar(32) DEFAULT NULL COMMENT '上一次操作人',
//        `PRE_TIME` datetime DEFAULT NULL COMMENT '上一次操作时间',
//        `OPERATION_STATUS` int(11) DEFAULT NULL COMMENT '上下架操作状态 1-成功，2-失败',


        Db.tx(Connection.TRANSACTION_SERIALIZABLE,()->{
            Db.save("pe_m_p_audiostream_episode",epi4VoiceRecord);
            Db.save("pe_m_p_on_sale_audiostream_episode",onSaleVoiceEpiRecord);
            Db.save("pe_m_p_audiostream_episode_status",voiceEpi4StatusRecord);
            return true;
        });

    }



    public static DataSource initDBConnect(String dbSource){
        String jdbcUrl ="jdbc:mysql://127.0.0.1:3306/cnpedb?verifyServerCertificate=false&useSSL=false&requireSSL=false&useUnicode=true&characterEncoding=UTF-8&rewriteBatchedStatements=true&useAffectedRows=true\n";
        if("local".equals(dbSource)){
        }
        if("test".equals(dbSource)){
            jdbcUrl="jdbc:mysql://192.168.10.111:3306/cnpedb?verifyServerCertificate=false&useSSL=false&requireSSL=false&useUnicode=true&characterEncoding=UTF-8&rewriteBatchedStatements=true&useAffectedRows=true\n";
        }
        DruidPlugin dp = new DruidPlugin(jdbcUrl, "root", "123456");
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        // arp.addMapping("blog", Blog.class);

        // 与 jfinal web 环境唯一的不同是要手动调用一次相关插件的start()方法
        dp.start();
        arp.start();
        return  new SimpleDataSource(jdbcUrl,"root","123456");
    }

}
