package com.wqg.mylop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class MylopApplication {

    public static void main(String[] args) {
        SpringApplication.run(MylopApplication.class, args);
    }

}
