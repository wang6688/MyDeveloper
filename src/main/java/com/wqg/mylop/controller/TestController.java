package com.wqg.mylop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@Controller
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/test")
    public String index() {
        return "test";
    }

    @RequestMapping("/videoDetail")
    public String videoDetail() {
        return "videoDetail";
    }

    @RequestMapping(value = "/message", method = RequestMethod.GET)
    public String messages(Model model) {
        model.addAttribute("messages","1111");
        System.out.println("1214214");
        return "dplayer-demo";
    }
}
